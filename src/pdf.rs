pub mod pdf {
    #[derive(Clone)]
    pub struct PDF {
        pub version: String,
        pub xref: CrossReference,
    }

    #[derive(Clone)]
    pub struct IndirectReference {
        pub id: usize,
        pub gen: usize,
    }

    #[derive(Clone)]
    pub struct CrossReference {
        pub table: CrossReferenceTable,
        pub trailer: CrossReferenceTrailer,
        pub byteoffset: usize,
    }

    #[derive(Clone)]
    pub struct CrossReferenceTable {
        pub count: [usize; 2],
        pub contents: Vec<CrossReferenceRow>,
    }

    #[derive(Clone)]
    pub struct CrossReferenceRow {
        pub offset: usize,
        pub gen: usize,
        pub t: String,
    }

    #[derive(Clone)]
    pub struct CrossReferenceTrailer {
        pub size: usize,
        pub root: Catalog,
    }

    #[derive(Clone)]
    pub struct Pages {
        pub idr: IndirectReference,
        pub kids: Vec<Page>,
    }

    #[derive(Clone)]
    pub struct Page {
        pub idr: IndirectReference,
        pub parent: IndirectReference,
        // [left, bottom, right, top]
        pub mediabox: [usize; 4],
        pub contents: Contents,
        pub resources: Resources,
    }

    #[derive(Clone)]
    pub struct Contents {
        pub idr: IndirectReference,
        pub stream: String,
        pub size: usize,
    }

    #[derive(Clone)]
    pub struct Resources {
        pub procset: ProcSet,
        pub font: Font,
    }

    #[derive(Clone)]
    pub struct ProcSet {
        pub idr: IndirectReference,
        pub elements: Vec<String>,
    }

    #[derive(Clone)]
    pub struct Font {
        pub idr: IndirectReference,
        pub subtype: String,
        pub name: String,
        pub basefont: String,
        pub encoding: String,
    }

    #[derive(Clone)]
    pub struct Catalog {
        pub idr: IndirectReference,
        pub outlines: Outlines,
        pub pages: Pages,
    }

    #[derive(Clone)]
    pub struct Outlines {
        pub idr: IndirectReference,
        pub outlines: Vec<Outline>,
    }

    #[derive(Clone)]
    pub struct Outline {
        // idr: IndirectReference,
    }

    #[derive(Clone, Debug)]
    pub struct Cursor {
        pub x: usize,
        pub y: usize,

        pub max_x: usize,
        pub max_y: usize,
    }

    pub trait PDFObject {
        fn read(self) -> String;
    }

    impl PDFObject for PDF {
        fn read(self) -> String {
            format!(
                "
    %PDF-{}
    {}
    {}
    %%EOF",
                self.version.clone(),
                self.xref.clone().trailer.root.clone().read(),
                self.xref.clone().read()
            )
        }
    }

    impl PDFObject for CrossReference {
        fn read(self) -> String {
            format!(
                "
    xref
    {}
    {}
    {}",
                self.table.clone().read(),
                self.trailer.clone().read(),
                self.byteoffset
            )
        }
    }

    impl PDFObject for CrossReferenceTrailer {
        fn read(self) -> String {
            format!(
                "
    trailer
    \t<< /Size {}
    \t   /Root {}
    \t>>
    startxref",
                self.size,
                get_reference_from_idr(self.root.idr),
            )
        }
    }

    impl PDFObject for CrossReferenceTable {
        fn read(self) -> String {
            let mut rows = format!("{} {}", self.count[0], self.count[1]);

            for row in self.contents.iter() {
                rows = format!("{}\n{}", rows, row.clone().read());
            }

            rows
        }
    }

    impl PDFObject for CrossReferenceRow {
        fn read(self) -> String {
            format!("{:0>9} {:0>5} {}", self.offset, self.gen, self.t)
        }
    }

    impl PDFObject for Catalog {
        fn read(self) -> String {
            format!(
                "
    {}\n\
    \t<< /Type /Catalog
    \t   /Outlines {}
    \t   /Pages {}
    \t>>
    endobj
    {}
    {}",
                self.idr.clone().read(),
                get_reference_from_idr(self.outlines.clone().idr),
                get_reference_from_idr(self.pages.clone().idr),
                self.outlines.clone().read(),
                self.pages.clone().read(),
            )
        }
    }

    impl PDFObject for Outlines {
        fn read(self) -> String {
            format!(
                "
    {}\n\
    \t<< /Type /Outlines
    \t   /Count {}
    \t>>
    endobj
            ",
                self.idr.clone().read(),
                self.outlines.len()
            )
        }
    }

    impl PDFObject for Pages {
        fn read(self) -> String {
            let mut k_ref = String::from("[");
            let mut k_string = String::new();

            for k in self.kids.clone().iter() {
                k_ref = format!("{}{},", k_ref, get_reference_from_idr(k.clone().idr));
                k_string = format!("{}{}", k_string, k.clone().read());
            }

            k_ref = String::from(&k_ref[..(k_ref.len() - 1)]); // can't have trailer comma
            k_ref = format!("{}]", k_ref);

            format!(
                "
    {}\n\
    \t<< /Type /Pages
    \t/Kids {}
    \t/Count {}
    \t>>
    endobj
    {}",
                self.idr.clone().read(),
                k_ref,
                self.kids.clone().len(),
                k_string
            )
        }
    }

    impl PDFObject for Page {
        fn read(self) -> String {
            format!(
                "
    {}\n\
    \t<< /Type /Page
    \t   /Parent {}
    \t   /MediaBox [{} {} {} {}]
    \t   /Contents {}
    \t   /Resources <<
    {}
    \t   >>
    \t>>
    endobj
    {}
    {}
    {}",
                self.idr.clone().read(),
                get_reference_from_idr(self.parent.clone()),
                self.mediabox[0],
                self.mediabox[1],
                self.mediabox[2],
                self.mediabox[3],
                get_reference_from_idr(self.contents.clone().idr),
                self.resources.clone().read(),
                self.contents.clone().read(),
                self.resources.clone().procset.clone().read(),
                self.resources.clone().font.clone().read()
            )
        }
    }

    impl PDFObject for Contents {
        fn read(self) -> String {
            format!(
                "
    {}
    << /Length {} >>
    stream
    {}
    endstream
    endobj
                ",
                self.idr.clone().read(),
                self.size,
                self.stream
            )
        }
    }

    impl PDFObject for Resources {
        fn read(self) -> String {
            format!(
                "\
    /ProcSet {}\n\
    /Font << /{} {} >>",
                get_reference_from_idr(self.procset.clone().idr),
                self.font.clone().name,
                get_reference_from_idr(self.font.clone().idr),
            )
        }
    }

    impl PDFObject for Font {
        fn read(self) -> String {
            format!(
                "\
    {}\n\
    \t<< /Type /Font\n\
    \t   /Subtype /{}\n\
    \t   /Name /{}\n\
    \t   /BaseFont /{}\n\
    \t   /Encoding /{}\n\
    \t   >>\n\
    endobj\n
            ",
                self.idr.clone().read(),
                self.subtype,
                self.name,
                self.basefont,
                self.encoding
            )
        }
    }

    impl PDFObject for ProcSet {
        fn read(self) -> String {
            let mut string = String::from("[");

            for el in self.elements.iter() {
                string = format!("{} /{}", string, el);
            }

            string = format!("{}{}", string, String::from("]"));

            format!(
                "
    {}
    \t{}
    endobj
    ",
                self.idr.clone().read(),
                string,
            )
        }
    }

    impl PDFObject for IndirectReference {
        fn read(self) -> String {
            format!("{} {} obj", self.id, self.gen)
        }
    }

    fn get_reference_from_idr(idr: IndirectReference) -> String {
        format!("{} {} R", idr.id, idr.gen)
    }

    pub fn generate_pdf(mediabox_coords: [usize; 4], stream: String) -> PDF {
        let font_helvetica = Font {
            idr: IndirectReference { id: 7, gen: 0 },
            subtype: String::from("Type1"),
            name: String::from("F1"),
            basefont: String::from("Helvetica"),
            encoding: String::from("MacRomanEncoding"),
        };

        let procset = ProcSet {
            idr: IndirectReference { id: 6, gen: 0 },
            elements: vec![String::from("PDF"), String::from("Text")],
        };

        let resources = Resources {
            procset: procset.clone(),
            font: font_helvetica.clone(),
        };

        let contents = Contents {
            idr: IndirectReference { id: 5, gen: 0 },
            stream: String::from(stream),
            size: 73,
        };

        let idr_pages = IndirectReference { id: 3, gen: 0 };

        let page = Page {
            idr: IndirectReference { id: 4, gen: 0 },
            mediabox: mediabox_coords,
            contents: contents.clone(),
            resources: resources.clone(),
            parent: idr_pages.clone(),
        };

        let pages = Pages {
            idr: idr_pages.clone(),
            kids: vec![page],
        };

        let outlines = Outlines {
            idr: IndirectReference { id: 2, gen: 0 },
            outlines: vec![],
        };

        let catalog = Catalog {
            idr: IndirectReference { id: 1, gen: 0 },
            outlines: outlines.clone(),
            pages: pages.clone(),
        };

        let xref_trailer = CrossReferenceTrailer {
            size: 8,
            root: catalog.clone(),
        };

        let mut xref_table = CrossReferenceTable {
            count: [0, 8],
            contents: vec![],
        };

        let offsets = [0, 9, 74, 120, 179, 364, 466, 496];

        for i in 0..8 {
            let mut gen = 0;
            let mut t = "n".to_owned();

            if i == 0 {
                gen = 65535;
                t = "f".to_owned();
            }

            let new_row = CrossReferenceRow {
                gen: gen,
                offset: offsets[i],
                t: t,
            };

            xref_table.contents.push(new_row);
        }

        let xref = CrossReference {
            table: xref_table.clone(),
            trailer: xref_trailer.clone(),
            byteoffset: 625,
        };

        let out_pdf = PDF {
            version: String::from("1.4"),
            xref: xref.clone(),
        };

        out_pdf.clone()
    }
}
