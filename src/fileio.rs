pub mod fileio {
    use crate::pdf;

    use pdf::pdf::{PDFObject, PDF};
    use std::env;
    use std::fs::{write, File};
    use std::io;
    use std::io::prelude::*;
    use std::process;

    pub fn read_file_from_argv() -> String {
        let args: Vec<String> = env::args().collect();

        if args.len() == 1 {
            println!("cargo run [input file]");
            process::exit(1);
        }

        let file_path: &String = &args[1];
        let mut file = match File::open(file_path) {
            Ok(f) => f,
            Err(e) => panic!("{}", e),
        };
        let mut contents = String::new();
        match file.read_to_string(&mut contents) {
            Ok(_) => println!("File loaded"),
            Err(e) => panic!("{}", e),
        };

        contents
    }

    pub fn sanitise_string(dirty_text: &String) -> String {
        str::replace(dirty_text, "\r", "")
    }

    pub fn write_pdf(out_pdf: &PDF, out_path: String) -> io::Result<()> {
        write(out_path, out_pdf.clone().read())
    }
}
