mod fileio;
mod pdf;
mod tokeniser;

pub use fileio::fileio::*;
pub use pdf::pdf::*;
pub use tokeniser::tokeniser::*;

fn main() {
    let raw = read_file_from_argv();
    let sanitised: String = sanitise_string(&raw);

    /*At the moment, I make the (naive) assumtion element n's child is
    element n + 1.*/
    let tokens = get_tokens(sanitised);
    println!("Found {} tokens!", tokens.len());

    let mediabox_coords: [usize; 4] = [0, 0, 612, 792];

    let mut cursor = Cursor {
        x: mediabox_coords[2] / 2,
        y: mediabox_coords[3] - 100,

        max_x: mediabox_coords[2],
        max_y: mediabox_coords[3],
    };

    let mut heading_stream = String::new();

    for token in tokens {
        match token {
            Token::TH { contents } => {
                println!("{}", contents);
                heading_stream = String::from(format!(
                    "  BT\n\t/F1 24 Tf\n\t{} {} Td\n\t({}) Tj\n  ET",
                    cursor.x, cursor.y, contents,
                ));
            }
            Token::PP { contents } => {
                println!("{}", contents);
                cursor.x = 0;
                cursor.y += 50;

                heading_stream = String::from(format!(
                    "  BT\n\t/F1 24 Tf\n\t{} {} Td\n\t({}) Tj\n  ET",
                    cursor.x, cursor.y, contents,
                ));
            }
            Token::SH { contents } => {
                println!("{}", contents);
            }
        };
    }

    let blank_pdf: PDF = generate_pdf(mediabox_coords.clone(), heading_stream);

    write_pdf(&blank_pdf, "res/blank.pdf".to_string()).expect("Could not write pdf");
}
