pub mod tokeniser {
    use regex::Regex;

    #[derive(Debug)]
    pub enum Token {
        TH { contents: String },
        SH { contents: String },
        PP { contents: String },
    }

    pub fn get_next_token(buff: &String) -> Result<(String, String, String), String> {
        let token_regex = Regex::new(r"^\.[A-Z]{1,2}$").unwrap();

        let mut contents = String::new();
        let mut rest = String::new();
        let mut token = String::from("");

        for line in buff.split('\n').rev() {
            if token != "" {
                if line.len() > 0 && &line[line.len() - 1..line.len()] != "\n" {
                    let mut line_fixed = String::from(line);
                    line_fixed.insert_str(line.len(), "\n");
                    rest.insert_str(0, &line_fixed);
                } else {
                    rest.insert_str(0, line);
                }
                continue;
            }

            if token_regex.is_match(line) {
                token = line.to_string();
            } else {
                let mut line_with_newline = String::from(line);
                if line.len() > 0 {
                    // because we split on \n we need to add it back
                    line_with_newline.insert_str(line.len(), "\n");
                }
                contents.insert_str(0, &line_with_newline);
            }
        }

        if token == "" {
            return Err("Could not find next token".to_string());
        } else {
            return Ok((token, contents, rest));
        }
    }

    pub fn get_tokens(mut buff: String) -> Vec<Token> {
        let mut tokens: Vec<Token> = Vec::new();
        loop {
            match get_next_token(&buff) {
                Ok((tok, cont, rest)) => {
                    buff = rest;

                    let this_token;
                    match &tok[..] {
                        ".TH" => this_token = Token::TH { contents: cont },
                        ".SH" => this_token = Token::SH { contents: cont },
                        ".PP" => this_token = Token::PP { contents: cont },
                        _ => panic!("Unrecognised token found: {}", tok),
                    }
                    tokens.push(this_token);
                }
                Err(e) => {
                    /*
                    TODO: This could really be better checked, maybe some
                    sort of Error enum?
                    */
                    if e == "Could not find next token" {
                        println!("Could not find next token");
                        break;
                    } else {
                        panic!("{}", e);
                    }
                }
            }
        }
        tokens.reverse();
        tokens
    }
}
